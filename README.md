We are pleased to welcome new and long-time patients from Bay Harbor, Bal Harbour, Surfside, Aventura, North Bay Village, Indian Creek, Sunny Isles and the surrounding area. Our goal is to ensure that our patients are happy and relaxed here and that they leave with a smile they can be proud of.

Address: 1048 Kane Concourse, Bay Harbor Islands, FL 33154, USA

Phone: 305-912-6473

Website: https://www.bayharbordentistry.com